<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', 'AuthController@register');
Route::post('/login', 'AuthController@login');
Route::post('/usersList', 'UsersListController@getUsersList');
Route::post('/createPost', 'PostsController@createPost');
Route::post('/showPosts', 'PostsController@showPosts');
Route::post('/deletePost', 'PostsController@deletePost');
Route::get('/updatePost/{id}', 'PostsController@updatePost');
Route::post('/editPost', 'PostsController@editPost');
Route::get('/editUser/{id}', 'AdminPossibilitiesController@editUser');
Route::post('/updateUser', 'AdminPossibilitiesController@updateUser');
Route::post('/deleteUser', 'AdminPossibilitiesController@deleteUser');

