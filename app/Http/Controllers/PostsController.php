<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Posts;
use App\User;
use Illuminate\Support\Facades\File;
class PostsController extends Controller
{
    public function createPost(Request $request) {
        $validate = $this->postValidation($request);
        $imageValidation = $this->imageValidation($request);
        if ($validate && $imageValidation) {
            $post = $request->all();
            $addImage = $this->addImage($post['file']);
            $post['image_name'] = $addImage;
            $postsTable = new Posts($post);
            $postsTable->save();
            return response()->json([
                'success' => true
            ]);
        }
    }

    public function showPosts(Request $request) {
        $userId = $request->userId;
        $isAdmin = User::select('admin')->where('id', $userId)->first();
        $posts = Posts::all();
        return ['isAdmin' => $isAdmin, 'posts' => $posts];
    }

    public function deletePost(Request $request) {
        $postDelete = $request->all();

        $isDeleted = $this->deleteImage($postDelete['imageName']);
        if ($isDeleted) {
            $deletedImage = Posts::findOrFail($postDelete['postId']);
            $deletedImage->delete();
            return response()->json([
                'success' => true
            ]);
        }
    }

    public function updatePost($id) {
        $updatePost = Posts::findOrFail($id);
        return $updatePost;
    }

    public function editPost(Request $request) {
        $editedPost = $request->all();
        $validation = $this->postValidation($request);
        if (gettype($editedPost['file']) != 'string') {
            $imageValidation = $this->imageValidation($request);
        }else {
            $edit = Posts::where('id', $editedPost['id'])->update(['title' => $editedPost['title'], 'description' => $editedPost['description']]);
            return $edit;
        }
        if ($validation && $imageValidation) {
            $addImage = $this->addImage($editedPost['file']);
            $isdeleted = $this->deleteImage($editedPost['imageName']);
            if ($isdeleted) {
                $editedPost['image_name'] = $addImage;
                $edit = Posts::findOrFail($editedPost['id']);
                $edit->update($editedPost);
                return $edit;
            }
        }
    }

    private function postValidation($request) {
        return $this->validate($request, [
            'title' => 'required|max:255',
            'description'=>'required',
        ]);
    }

    private function imageValidation($file) {
        return $this->validate($file, [
            'file' => 'required|mimes:png,jpeg,jpg,gif|max:2048'
        ]);
    }

    private function deleteImage($imageName) {
        $deletedUrl = public_path('uploadImages\\').$imageName;
        if (File::exists($deletedUrl)){
            File::delete($deletedUrl);
            return true;
        }else {
            return false;
        }
    }

    private function addImage($postFile) {
        $imageName = time().'.'.$postFile->getClientOriginalExtension();
        $postFile->move(public_path('uploadImages'), $imageName);
        return $imageName;
    }
}
