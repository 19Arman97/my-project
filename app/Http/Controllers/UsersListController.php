<?php

namespace App\Http\Controllers;

use App\User;
class UsersListController extends Controller
{
    public function getUsersList() {
        return User::all();
    }
}
