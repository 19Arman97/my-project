<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class AdminPossibilitiesController extends Controller
{
    public function editUser($id) {
        $updateUser = User::findOrFail($id);
        return $updateUser;
    }

    public function updateUser(Request $request) {
        $this->userValidate($request);
        $updateUser = $request->all();

        $edit = User::findOrFail($updateUser['id']);
        $updateUser['password'] = bcrypt($request->password);
        $edit->update($updateUser);
        return $edit;
    }

    public function deleteUser(Request $request) {
        $deletedUser = User::findOrFail($request['userId']);
        $deletedUser->delete();
        return response()->json([
            'success' => true
        ]);
    }

    private function userValidate($request) {
        return $this->validate($request, [
            'email' => 'email',
            'password' => 'required|between:6,25|confirmed'
        ]);
    }

}
