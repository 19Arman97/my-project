<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;

class AuthController extends Controller
{

    public function register(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|between:6,25|confirmed'
        ]);

        $user = new User($request->all());
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json([
            'success' => true
        ]);
    }

    public function login(Request $request) {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|between:6,25'
        ]);

        $user = User::select('*')->where('email', $request->email)->first();
        if($user && Hash::check($request->password, $user->password)) {
            $user->save();
            return response()->json([
                'success' => true,
                'user' => $user,
            ]);
        }

        return response()->json([
            'errors' => [
                'authorizationError' => 'These credentials do not match our records.'
            ]
        ]);
    }

}
