import VueRouter from 'vue-router';
import RegisterComponent from './components/RegisterComponent';
import LoginComponent from './components/LoginComponent';
import ProfileComponent from './components/ProfileComponent';
import HomeComponent from './components/HomeComponent';
import UsersListComponent from "./components/UsersListComponent";
import CreatePostComponent from "./components/CreatePostComponent";
import UpdatePostComponent from "./components/UpdatePostComponent";
import EditUserComponent from "./components/EditUserComponent";
import AddUserComponent from "./components/AddUserComponent";

export default new VueRouter({
    routes: [
        {
            path: '/register',
            component: RegisterComponent,
            name: 'register'
        },
        {
            path: '/login',
            component: LoginComponent,
            name: 'login'
        },
        {
            path: '/profile',
            component: ProfileComponent,
            name: 'profile'
        },
        {
            path: '/',
            component: HomeComponent,
            name: 'home'
        },
        {
            path: '/usersList',
            component: UsersListComponent,
            name: 'usersList'
        },
        {
            path: '/createPost',
            component: CreatePostComponent,
            name: 'createPost'
        },
        {
            path: '/updatePost/:id',
            component: UpdatePostComponent,
            name: 'updatePost'
        },
        {
            path: '/editUser/:id',
            component: EditUserComponent,
            name: 'editUser'
        },
        {
            path: '/addUser/',
            component: AddUserComponent,
            name: 'addUser'
        },
    ],
    mode: 'history'
})
